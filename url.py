# -*- coding: utf-8 -*-


class URL(object):

    @staticmethod
    def url_is_direct(url):
        url_end = url[len(url) - 4:]
        return url_end == ".jpg" or url_end == ".gif" or url_end == ".png"

    @staticmethod
    def url_is_indirect(url):
        if not URL.url_is_direct(url) and not URL.url_is_album(url):
            return True

        return False

    @staticmethod
    def url_is_album(url):
        return url[16:19] == "/a/" or url[18:21] == "/a/"

    @staticmethod
    def trim_extension(url):
        try:
            garbage_index = url.index("?")
            return url[:garbage_index]
        except ValueError:
            return url

    @staticmethod
    def trim_album(url):
        if "?" in url:
            url = url[:url.index("?")]
            print "removed ?"

        if "#" in url:
            url = url[:url.index("#")]
            print "removed #"

        return url

    @staticmethod
    def convert_miniature_to_full_image(url):
        extension = url[len(url) - 4:]
        return url[:len(url) - 5] + extension

